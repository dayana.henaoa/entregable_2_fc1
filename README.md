# entregable_2_fc1

## Tabla de Contenidos

- [Instalación](#instalación)
- [Uso](#uso)
- [Resultados](#resultados)
- [Contribuir](#contribuir)

## Instalación

Para instalar este proyecto, simplemente es necesario hacer lo siguente:

1. **Fork del Repositorio:** Haz clic en el botón "Fork" en la parte superior derecha de esta página para copiar este repositorio a tu cuenta de GitLab.

2. **Clonar el Repositorio:** Una vez que hayas creado tu fork, copia la URL del repositorio desde la página de tu fork. Luego, en tu terminal, ejecuta el siguiente comando para clonar el repositorio a tu máquina local:

   ```bash
   git clone https://gitlab.com/dayana.henaoa/entregable_2_fc1.git

## Uso

El proyecto, llamado "Game Of Life", consiste en la implementación del "Juego de la Vida" con dos especies celulares, siguiendo las condiciones o reglas de juego que se definen en la función process_life. Estas condiciones son de vida, muerte, supervivencia y competencia y, básicamente, detallan cómo evolucionan las células en un tablero bidimensional en función de su entorno cercano. Estas condiciones se podrían cambiar, igual que la proporción entre células vivas del tipo A y células vivas del tipo B, en caso de querer analizar otras formas de evolución del sistema. 

Posteriormente, se obtiene el histograma normalizado del número de células A y B para las 1000 iteraciones del juego, utilizando herramientas como matplotlib.

Es importante considerar el sistema como descrito por los estados NA y NB, donde NA es el número de células tipo A y NB es el número de células tipo B. Se debe tener en cuenta que NA+NB debe ser menor o igual al número total de casillas del tablero (NT). Además, las células A y B están correlacionadas, por lo que se debe utilizar una distribución de probabilidad 2-dimensional para los estados del sistema.

Utilizando las distribuciones obtenidas, se debe aplicar el algoritmo de Metrópolis para generar una serie de estados del sistema. Finalmente, se grafica la probabilidad 2-dimensional de los estados generados con el algoritmo de Metrópolis y se muestra cualitativamente que la distribución obtenida es similar a la deseada.

Este proyecto permitirá explorar el comportamiento del "Juego de la Vida" con dos especies celulares y analizar cómo diferentes modificaciones en las reglas afectan la dinámica del sistema.




## Resultados 
Con este codigo se puede obtener una analogía entre el manejo del algoritmo de la vida y una implementación de este haciendo uso del algoritmo metropolis, se presenta como una base didactica que permite encontrar las similitudes y las aplicaciones de ambos algoritmos, a su vez, los resultados obtenidos son favorables con lo esperado dado las distribuciones de probabilidad obtenidas con ambos algoritmos son similares, lo que quiere decir que los condicionales de metropolis sirven también para la simulación de una población de tipo automata celular. 

**Los resultados en forma de imagenes se encuentran en la carpeta "Results"

## Autores y contribución
Este trabajo se realizó en equipo por las siguientes contribuyentes:
Sara Carvajal (saraa.carvajal@udea.edu.co) , Dayana Henao (dayana.henaoa@udea.edu.co), Valentina Lobo (valentina.lobo@udea.edu.co)
